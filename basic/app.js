 var Comment = React.createClass({
       getInitialState:function(){
    return {editing:false}
    },
          edit:function(){
        this.setState({editing:true});
    },
      remove:function(){
        alert('Remove Comment');
    },
      save:function(){
      var val = this.refs.newText.value;
      console.log('new comment:' + val);
      this.setState({editing:false})
    },
     renderNormal:function(){
 return (
         <div className="commentContainer">
            <h1 className="commentText">{this.props.children}</h1>
        <button onClick={this.edit} className="btn-primary">edit</button>
        <button onClick={this.remove}  className="btn-danger">Remove</button>
    </div>
        );
},
  renderForm:function(){
 return (
         <div className="commentContainer">
            <textarea ref='newText' defaultValue={this.props.children}></textarea>
        <button onClick={this.save}  className="btn-success">Save</button>
    </div>
        );
},
        render:function(){
         if(this.state.editing){
         return this.renderForm();
     }else{
      return this.renderNormal();
 }
    }

    });

var Board = React.createClass({

  getInitialState: function(){
    return {
      comments:[
                'Hello',
                'World',
                'Okay'
        ]
      }
    },
removeComment: function(i){
console.log('Remove comment: ' + i);
var arr = this.state.comments;
arr.splic(i, 1);
this.setState({comments:arr});
},
updateComment:function (i){
  console.log('Upate comment: ' + i);
  var arr = this.state.comments;
  arr[i] = newText;
  this.setState({comments:arr});
},
eachComment: function(text,i){
  return (
  <Comment key={i} index={i}>
  {text}
  </Comment>)
  ;
},
render:function(){
  return (
  <div className="boardContainer">
   {
    this.state.comments.map(this.eachComment)
   }
 </div>
 );
}

});

ReactDOM.render(
            <div className="board">
               <Board>Hey </Board>
                <Comment>Hey remove content</Comment>
                 <Comment>Hey edit</Comment>
            </div>
            , document.getElementById('container')
        );