------------
# Command Line
------------
- cd desktop (inside goto folder)
- cd .. (previous folder)
- dir (all show list)
- dir Desktop/test (list)
- dir /a (all hidden files show)
- cls (clear screen)
- dir *.png (all show png)
- any file name (hit enter then open file)
- ipconfig /? (show help list network)
- mkdir FolderName (create Folder)
- rmdir FolderName (remove Folder only one folder)
- rmdir /s (all folder Remove parent and child)
- path (environment path list)

----------------------------
# Drives and Changing Colors
----------------------------
- wmic logicaldisk get name (all drive name list)
- tree (all list show like tree)
- color 0B
- color /? (all color name show help)

----------------------------
# Files Attribute
----------------------------
- attrib /? (help)
- attribe +h test.txt (hidden file)

----------------------------
# Deleting and Appending to Files
----------------------------
- del test.txt
- echo Hello WOrld > test.txt (text write in txt file)
- type test.txt (show the data in file)
- dir > test.txt (print drive information in text file)
- copy text.txt folderName (data copy)
- copy text.txt folderName e: (data copy in drive)
- xcopy FolderName to  folderName (data cut )
- xcopy FolderName to  folderName /s (data all cut )
- move FoldetName (data move)
- rename FoldetName renameFolderName (folder rename or file rename)