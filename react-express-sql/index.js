const express = require('express');
const cors = require('cors');
var mysql = require('mysql');

const app = express();

const SELECT_ALL_PRODUCTS_QUERY = 'SELECT * FROM products';

const connection = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "",
    database:'cishop'
  });
  
  connection.connect(err => {
    if (err) throw err;
    console.log("Connected!");
  });

app.use(cors());

const port = 4000;

//console.log(connection);

app.get('/', (req, res) => {
    res.send('go to /products to see products');
    // res.json(products);
   
});

app.get('/products/add', (req, res) => {
    const {title,price} = req.query;
    console.log(title,price);
    const INSERT_PRODUCT_QUERY = `INSERT INTO products (title, price) VALUES ('${title}', ${price})`;
connection.query(INSERT_PRODUCT_QUERY, (err,results) => {
    if(err){
       return res.send(err); 
    }else{
        return res.send('Successfully Added Product'); 
    }
})
    
});
app.get('/products', (req, res) => {
    connection.query(SELECT_ALL_PRODUCTS_QUERY,(err,results) =>{
        if(err){
           return res.send(err); 
        }else{
            return res.json({
                data:results
            });
        }
    })
});

app.listen(port, () => console.log(`Example app listening on port ${port}!`));