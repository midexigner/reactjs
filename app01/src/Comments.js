import React, { Component } from 'react';

function Message(props){
    if(!props.message){
        return null;
    }
    return(
    <div>
        <h4> Comment Added.....</h4>
    </div>

)

}

class CommentForm extends Component{
    constructor(props){
        super(props);
        this.state = {message:false};
        this.commentSubmit = this.commentSubmit.bind(this);
     }  

     commentSubmit(e){
         e.preventDefault();
         this.setState({message:true});
     }
    render(){
        return(
            <div>
                <Message message={this.state.message}/>
                <h2>Add a comment.....</h2>
                <form>
                    <textarea name="comment" placeholder='Comments' cols="40" rows="5"></textarea>
                    <div>
                        <button onClick={this.commentSubmit}>Add a Comment</button>
                    </div>
                </form>
            </div>
        )
    }
}

class Comments extends Component {
    constructor(props){
        super(props);
        this.state = {isLoggedIn:false};
        this.LoginButton = this.LoginButton.bind(this);
        this.LogoutButton = this.LogoutButton.bind(this);
       
     }
     LoginButton(){
         this.setState({isLoggedIn:true});
     }

     LogoutButton(){
        this.setState({isLoggedIn:false});
    }

     render(){
         const isLoggedIn = this.state.isLoggedIn;
        const commentsList = ['comment Message 1','Comment Mesage 2'];
         let commentForm;
         if(isLoggedIn){
            commentForm = <CommentForm/>;
         }else{
            commentForm = "Please login then write the comments"
         }
        return(
        <div className='content-info'>
        <button onClick={this.state.isLoggedIn ? this.LogoutButton:this.LoginButton}>{this.state.isLoggedIn ? 'Logout':'Login'}</button><br/>
        {commentForm}
        {commentsList.length > 0 && <div>
        <h3>Comments</h3>
        <p>{commentsList[0]}</p>
        <p>{commentsList[1]}</p>
        </div>}
       </div>
        )    
        }

}

export default Comments;