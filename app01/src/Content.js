import React, { Component } from 'react';

class Content extends Component {
    constructor(props){
        super(props);
        this.state = {iShow: true};
        //this.showHideContent = this.showHideContent.bind(this); // bind event
       
     }
//      showHideContent(id){
//          console.log(id);
//     this.setState(state => (
//         {iShow: !state.iShow}  
//     ))
// }
showHideContent = (id) => {
    console.log(id);
    this.setState(state => ( 
        {iShow: !state.iShow}  
      ))
}
     render(){
        return(
        <div className='content-info'>
        <button onClick={()=>this.showHideContent(99)}>{this.state.iShow ? 'Hide':'Show'} Content</button>
        {/* {()=>this.showHideContent(99)} */}
        {this.state.iShow && <div>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
        </div>}
        </div>
        )    
        }

}

export default Content;