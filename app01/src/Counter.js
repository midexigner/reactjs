import React, { Component } from 'react';

class Counter extends Component {

    constructor(props){
    super(props);
    this.state = {count:this.props.num};
    this.stateHandling = this.stateHandling.bind(this);
 }
 // Don't use like this
 /*
updateCount = () => (
    this.setState(
        {count: this.state.count + 1}  
    )
)*/
updateCount = () => (
    this.setState((state) => (
        {count: state.count + 1} 
    ))
)
stateHandling(){
   this.updateCount(); 
   this.updateCount();
   this.updateCount();
   this.updateCount();
}
    render(){
        return(
            <div className="Counter">
                <h2>{this.state.count}</h2>
                <button onClick={this.stateHandling}>Click Me</button>
            </div>
        )
    }
}

export default Counter;