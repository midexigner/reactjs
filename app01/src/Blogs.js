import React, { Component } from 'react';

function Posts(props){
    const listItems = props.posts.map((post,index)=>
    <PostItems post={post} key={post.id} />
    );
    return(
        <div className="posts">
    {listItems}
        </div>
    )
}

function PostItems(props){
    const post = props.post;
    return(
<div id={'post-'+post.id}>
        <h4>{post.title}</h4>
        <p>{post.content}</p>
    </div>
    );
}
class Blogs extends Component {
     render(){
        const posts = [
            {id:1,title:"Blog Post 1", content:"Blog post 1 content here...."},
            {id:2,title:"Blog Post 2", content:"Blog post 2 content here...."},
            {id:3,title:"Blog Post 3", content:"Blog post 3 content here...."},
            {id:4,title:"Blog Post 4", content:"Blog post 4 content here...."}
        ];
        return(
        <div className='blog-list'>
        <h2>Blog Posts</h2>
        <Posts posts={posts}/>
        </div>
        )    
        }

}

export default Blogs;