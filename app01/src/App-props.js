import React, { Component } from 'react';
import './App.css';
import Student from './Student';
import Product from './Product';
const pageTitle = "Welcome to my First React";

// json object
const student = {
  name: "Muhammad Idrees",
  course:{name:'Developer',duration:'6 Months'},
  fees:'1000'
}

const productsList = {
  name: "Doman Hosting",
  price:'5000'
}



/*const clickMe = () => alert("Hello");

function evaluate(a,b){
  if(a > b){
    return <span class='subtract'>{a - b }</span>;
  }else{
    return <span class="addition">{a + b }</span>;
  }
}*/
// this is props method
const Category = (props) => (
  <h2>Category Name: {props.name}</h2>
)
Category.defaultProps = {
  name:"Domain"
}
class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1>{pageTitle}</h1>
          <Category  />
         <Student  student={student}/>
         <Product productsListing={productsList}/>
         {/* <p>{evaluate(10,15)}</p>
                  <p>{evaluate(20,15)}</p>
                  <button onClick={clickMe}>clickMe</button>*/}
        </header>
      </div>
    );
  }
}

export default App;
