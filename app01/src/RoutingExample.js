import React from "react";
import { BrowserRouter as Router, Route, Link, Switch, Redirect } from "react-router-dom";
import ContactForm from './ContactForm';
function RoutingExample() {
  return(
    <div>
      <Router>
        <div>
          <Header />
          <hr />
          <Switch>
            <Route exact path="/" component={Home}/>
            <Route path="/about" component={About}/>
            <Route path="/service" component={Service}/>
            <Route path="/users" component={Users}/>
            <Route path="/contact" component={ContactForm}/>
            <Route component={NoMatch}/>
          </Switch>
        </div>
      </Router>
    </div>
  )
}

function Header() {
  return (
    <ul>
      <li><Link exact to="/">Home</Link></li>
      <li><Link to="/about">About</Link></li>
      <li><Link to='/service'>Service</Link></li>
      <li><Link to={{pathname: '/contact'}}> Contact</Link></li>
      <li><Link to="/users">Users</Link></li>
    </ul>
  )
}

function Home() {
  return(
    <div>
      <h1>Home</h1>
      <p>This is home page.</p>
    </div>
  )
}

function About() {
  return(
    <div>
      <h1>About</h1>
      <p>This is about page.</p>
    </div>
  )
}

function Service() {
  return(
    <div>
      <h1>Service</h1>
      <p>This is service page.</p>
    </div>
  )
}

function Users({match,location,history}) {
  return (
    <div>
      <ul>
        <li><Link to={`${match.url}/1`}>Idrees</Link></li>
        <li><Link to={`${match.url}/2`}>Bilal</Link></li>
      </ul>
      <Switch>
        <Route path={`${match.path}/:id`} component={User} />
        <Route exact path={match.path} render={() => (<h2>Please select user</h2>)}/>
      </Switch>
    </div>
  );
}

function User({match}) {
  return(
    <div>
      <h2>User Detail</h2>
      <p>user id is: {match.params.id}</p>
      {(parseInt(match.params.id) === '') ? (<Redirect to='/users' />) : ''}
    </div>
  )
}

function NoMatch() {
  return(
    <div>
      <h1>404 Error</h1>
      <p>Oops! page not found.</p>
    </div>
  )
}

export default RoutingExample;