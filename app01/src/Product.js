import React, { Component } from 'react';

class Product extends Component{
    constructor(props){
        super(props);
        this.state = {productId:'',qty:0,isCart:true};
    }

    addToCart = (pid)=>(
        this.setState((state) => (
            {productId: pid, qty: state.qty + 1} 
        ))
    );
    removeCart = () => (
        this.setState({isCart: false})
      );

render(){
return(
<div className='product-info'>
<h2>Product Name:{this.state.qty}</h2>
<button onClick={()=> this.addToCart(1)}>Add To cart</button>
<button onClick={()=> this.addToCart(2)}>Add To cart</button>
<button onClick={()=> this.addToCart(3)}>Add To cart</button>
<button onClick={()=> this.addToCart(4)}>Add To cart</button>
{/* <Cart productId = {this.state.productId} qty={this.state.qty} /> */}
{ this.state.isCart && <Cart productId = {this.state.productId} qty={this.state.qty} />}
        {! this.state.isCart && <h3>Cart has been removed</h3>}
       
        <button onClick={this.removeCart}>Remove Cart</button>
</div>
)    
}
}

class Cart extends Component{
    constructor(props){
        super(props);
        this.state = {qty:this.props.qty};
    }
static getDerivedStateFromProps(props,state){
if(props.qty !== state.qty){
    return {qty:props.qty}
}
return null;
}
componentDidMount(){
    console.log('Executer after componet render');
    // this.updateQty();
}
shouldComponentUpdate(nextProps,nextState){
    if(this.props.qty !== nextProps.qty){
       // return {qty:props.qty}
       console.log('Should Component Update');
       return true
    }
    return false;  
}
/* updateQty = () =>{
    this.setState((state) => (
        {qty: state.qty + 1} 
    ))
} */

componentDidUpdate(prevState, prevProps){
//console.log('Component Update');
if(this.props.productId !== prevProps.productId){
    console.log('Component Update');
}
}
componentWillUnmount() {
    console.log('component is unmounted and destroyed');
  }
    render(){
    return(
    <div>
    <h2>Cart item {this.state.qty}</h2>
    {/* <button onClick={this.updateQty}>Click Me</button> */}
    </div>
    )    
    }
    }

export default Product;