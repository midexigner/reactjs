import React, { Component } from 'react';


class ContactForm extends Component {
    constructor(props){
        super(props);
        this.initialState = {
          name:'',
          email:'',
          phone:'',
          message:''
        }
        this.state = this.initialState;
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
      }
handleChange(event){
    const name = event.target.name;
    const value = event.target.value;
    this.setState({
    [name]:value
    })
}
handleSubmit(event){
    event.preventDefault();
    this.props.onSubmit(this.state);
    this.setState(this.initialState);
   console.log(this.state);
}
     render(){
        
        return(
        <div className="container">
        <div className="row bg-light">
        <div className="col-lg-12">
        <h2 className="text-center text-black display-4 pt-2">Contact Form </h2>
        </div> 
        </div>
        <div className="row justify-content-center">
        <div className="col-sm-6">
       <form method="post">
       <div className="form-group">
           <label> Name:  </label>
             <input type="text" className="form-control" name="name" value={this.state.name} onChange={this.handleChange}/>
             </div>
             <div className="form-group">
           <label> Email:  </label>
             <input type="email" className="form-control" name="email" value={this.state.email} onChange={this.handleChange}/>
             </div>
             <div className="form-group">
           <label> Phone:  </label>
             <input type="tel" className="form-control" name="phone" value={this.state.phone} onChange={this.handleChange}/>
             </div>
             <div className="form-group">
           <label> Message:  </label>
           <textarea name="message" className="form-control" value={this.state.message} onChange={this.handleChange} />
             </div>
             <div>
             <button type="submit" className="btn btn-primary" onClick={this.handleSubmit}>Send</button>
             </div>
       </form>
       </div>
       </div>
        </div>
        )    
        }

}

export default ContactForm;