import React from 'react';
import { Table } from 'react-bootstrap';

class ProductList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      products: []
    }
  }

  componentDidMount() {
//const apiUrl = 'http://localhost/react/api/products.php?action=read';
const apiUrl = 'http://localhost/javascript/reactjs/api/products.php?action=read';
    fetch(apiUrl)
      .then(res => res.json())
      .then(
        (result) => {
            console.log(result);
                this.setState({
                    products: result.products
                  });
        },
        (error) => {
          this.setState({ error });
        }
      )
  }

  render() {
    const { error, products} = this.state;

    if(error) {
      return (
        <div>Error: {error.message}</div>
      )
    } else {
      return(
        <div>
          <h2>Product List</h2>
          <Table>
            <thead>
              <tr>
                <th>#ID</th>
                <th>Product Name</th>
                <th>SKU</th>
                <th>Price</th>
              </tr>
            </thead>
            <tbody>
                
              {products.map(product => (
                <tr key={product.id}>
                  <td>{product.id}</td>
                  <td>{product.product_name}</td>
                  <td>{product.sku}</td>
                  <td>{product.price}</td>
                  <td>{product.price}</td>
                </tr>
              ))}
            </tbody>
          </Table>
        </div>
      )
    }
  }
}

export default ProductList;