import React, { Component } from 'react';
function Course(props){
return(
<div className='course-detail'>
<h3>Course: {props.course.name}</h3>
<p>{props.course.duration}</p>
</div>
)
}
const Fee = (props) => (
  <div className='fee-detail'>
<h3>Fee: {props.fee}</h3>
</div>                     
)
class Student extends Component{
render(){
return(
<div className='student-info'>
<h2>Name: {this.props.student.name}</h2>
<Course course ={this.props.student.course} />
<Fee fee ={this.props.student.fees} />
</div>
)    
}
}

export default Student;