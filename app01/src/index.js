import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
/*
https://www.youtube.com/watch?v=whyhfBsYW-4&list=PLgOUQYMnO_SSI7kXWrm21_TqPCU1t451k&index=6
// Stateless components
function Student(props){
return(
<h2>Student Name: {props.name}</h2>
)
}
// Stateful Components
class Teacher extends React.Component{
render(){
 return(
   <div>
 <h2>Teacher Name: {this.props.name}</h2>
    <Student name="Muhammad Bilal" />
    <Student name="Ahmer" />
   </div>
  )
}
}
const element = <Teacher name="Muhammad Idrees" />;
ReactDOM.render(element, document.getElementById('root'));
*/
const domContainer = document.querySelector('#root');
//document.getElementById('root')
ReactDOM.render(<App />, domContainer);


// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
