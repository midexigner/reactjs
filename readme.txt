=> What you need to know
  - HTML
  - JavaScript
  - Unafarid of using the command line (very simple)

=> What is React?

- JavaScript framework for creating user interfaces
  -  Full single page web applications
  -  Just certain parts of a website (like a search form)

- Component-based
- Very fast (Thanks to virtual DOM)
- Created by Facebook

=> JavaScript components
 - search component
 - directory component
 - signup component

=> What we'll learn..
  - Installing React and setting up a workflow with webpack
  - Create a simple to-do app, looking at:
     - Components, props, state, events and more
  - Routing
  - create-react-app


-> node -v
-> npm --yes
-> npm i create-react-app
-> npx create-react-app app01
-> cd app01
-> yarn start

const pageTitle = React.createElement(
'h1',
{className:'page-title'},
'Welcome to my First Reac'
  );


  # install express
  - npm install express --save
  - npm install mysql
  - npm install cors
      or
  - yarn add express mysql cors body-parser

