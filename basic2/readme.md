- Comfortable with HTML5 and CSS3
- Aware of Bootstrap
- Comfortable with JavaScript
- Familiar with Node.js, specifically NPM
- Using React 0.13.2
- You'll use createClass not createComponent
- The transferPropsTo method has been deprecated
- Using /** @jsx React.DOM  ** / is not required.

-> React.js
- Is open source, but maintained by Facebook
- Can be the "V" (or view) in MVC
- Is ideal for large-scale, single page application
- Uses a high-speed virtual DOM
- Uses clean and easy-to-understand JSX syntax

-> why is React so fast?
- DOM (Document Object Model)
- The structure of HTML or XML elements that make up a web page.
- JS objects are faster than DOM objects
- The React virtual DOM is a JS object.
- React never reads from the "real" DOM.
- React only writes to the real DOM if needed.
- React efficiently handles DOM updates.

-> React Tools with Chrome
- https://chrome.google.com/webstore/category/extensions
- "react-detector","React Developer Tools"

-> ReactJS 0.13.3
- https://github.com/facebook/react/tree/0.13-stable (github 0.13)
- https://fb.me/react-0.13.3.min.js
- https://reactjs.org/ (latest Version)

- npm install -g react-tools
- jsx /src/ build/